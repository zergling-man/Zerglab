import utils as u

components={'Centre Torso':['ct'],'Right Torso':['rt','Right Shoulder','rs'],'Left Torso':['lt','Left Shoulder','ls'],'Left Leg':['ll'],'Right Leg':['rl'],'Left Arm':['la'],'Right Arm':['ra'],'Head':['h','he']}

class Component:
	def __init__(self, name='Centre Torso'):
		self.name=u.dealias(name) or name
		self.components=[]

class Mech:
	def __init__(self):
		self.name='New Mechiah'
		self.chassis='Hunchback'
		self.variant='4G'
		self.id=1
		self.c=self.components={'head':Component('h'),Component('la'),Component('ra'),Component